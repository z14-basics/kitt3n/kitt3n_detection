<?php

namespace KITT3N\Kitt3nDetection\UserFunc;

use Wolfcast\BrowserDetection;

class GetBrowserInfo
{
    const COOKIE_NAME_BROWSER_DETECTION = "sBrowserDetection";
    const COOKIE_NAME_MOBILE_DETECTION = "bMobileDetection";

    public static function getBrowserInfo()
    {
        $aCurrentApplicationContext = \TYPO3\CMS\Core\Utility\GeneralUtility::getApplicationContext();

        if ( ! isset($_COOKIE[self::COOKIE_NAME_BROWSER_DETECTION])) {
            $browser = new BrowserDetection();

            /*
            * Browser Agent
            */
            $sBrowserAgent = \TYPO3\CMS\Core\Utility\GeneralUtility::getIndpEnv('HTTP_USER_AGENT');

            $sClass = "";

            /*
             * Get Platform
             */
            $sOs = $browser->getPlatform();

            /*
             * Create Classes
             */
            $sClass .= ($sClass == '' ? '' : ' ') . "bMobile-" . ($browser->isMobile() ? '1' : '0');
            $sClass .= ($sClass == '' ? '' : ' ') . "bIOS-" . (($sOs == 'iPad') || ($sOs == 'iPhone') ? '1' : '0');

            $sClass .= ($sClass == '' ? '' : ' ') . "sOs-" . str_replace(" ", "-", $browser->getPlatform());
            // sOsV-macOS Mojave
            $sClass .= ($sClass == '' ? '' : ' ') . "sOsV-" . str_replace(" ", "-", $browser->getPlatformVersion());

            $sClass .= ($sClass == '' ? '' : ' ') . "sB-" . str_replace(" ", "-", $browser->getName());
            $sClass .= ($sClass == '' ? '' : ' ') . "sBV-" . str_replace(" ", "-", $browser->getVersion());

            /*
             * Set Cookie if application context != Development
             */
            if( ! $aCurrentApplicationContext->isDevelopment()){
                setcookie(self::COOKIE_NAME_BROWSER_DETECTION, $sClass);
            }
        } else {
            $sClass = $_COOKIE[self::COOKIE_NAME_BROWSER_DETECTION];
        }
        return $sClass;
    }

    public static function bMobile()
    {
        $aCurrentApplicationContext = \TYPO3\CMS\Core\Utility\GeneralUtility::getApplicationContext();

        if ( ! isset($_COOKIE[self::COOKIE_NAME_MOBILE_DETECTION])) {
            $browser = new BrowserDetection();

            /*
            * Mobile?
            */
            $bMobile =  ($browser->isMobile() ? '1' : '0');
            /*
             * Set Cookie if application context != Development
             */
            if( ! $aCurrentApplicationContext->isDevelopment()) {
                setcookie(self::COOKIE_NAME_MOBILE_DETECTION, $bMobile);
            }
        } else {
            $bMobile = $_COOKIE[self::COOKIE_NAME_MOBILE_DETECTION];
        }
        return $bMobile;
    }
}