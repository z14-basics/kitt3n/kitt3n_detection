<?php
declare(strict_types = 1);
namespace KITT3N\Kitt3nDetection\ExpressionLanguage;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use TYPO3\CMS\Core\ExpressionLanguage\AbstractProvider;
use KITT3N\Kitt3nDetection\ExpressionLanguage\Functions\IsMobileConditionFunctionsProvider;

/**
 * Scope: frontend
 * **This classis NOT meant to be sub classed by developers .**
 *
 * @internal
 */
class IsMobileConditionProvider extends AbstractProvider
{
    public function __construct()
    {
        $this->expressionLanguageProviders = [
            // ... and our custom function provider
            IsMobileConditionFunctionsProvider::class
        ];
    }
}
